import express from "express"
import { RouteRegistrator } from "../pkg"

export const registerStaticRoutes: RouteRegistrator = (app) => {
  app.use("/", express.static("public"))
  app.use("/", express.static("node_modules/normalize.css"))
  app.get("/tos", (_: Request, res: any) => res.render("tos"))
  app.get("/privacy-policy", (_: Request, res: any) => res.render("privacy-policy"))
}
